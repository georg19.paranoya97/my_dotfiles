let g:ycm_server_keep_logfiles = 1
let g:ycm_server_log_level = 'debug'
set shell=/bin/bash

let mapleader = ','

" dont warn if existing swap file is found
set shortmess+=A

" show line numbers
set number
" wrap lines visually. But don't add new lines to the file
set textwidth=0
set wrapmargin=0
" highlight matching brace
set showmatch
" display ruler
set ruler

" enable syntax highlighting
syntax on
" better cmd-line completion
set wildmenu

" instead of failing command, because of unsaved changes, raise dialogue
set confirm

" no bell (no beeping nor visual feedback)
set visualbell
set t_vb=

" Search as soon as characters are entered
set incsearch
" Highlight search results
set hlsearch

" set nocompatible
filetype off

" CATEGORIE: PLUGINS

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" Add your plugins below this line
Plugin 'scrooloose/nerdtree'
Plugin 'valloric/youcompleteme'

" All of your plugins must be added before the following line
call vundle#end()
filetype plugin indent on

" CATEGORIE: TABS & WHITESPACES

" Tabs are spaces
set expandtab
" Auto-indent new lines
set autoindent
" Auto-indent space witdth
set shiftwidth=4
" Enable smart-indent
set smartindent
" enable smart-tabs
set smarttab
" number of spaces per tab
set softtabstop=4
" allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" CATEGORIE: KEY BINDINGS

" new tab
nmap <leader>t :tab split<CR>
" close tab
nmap <leader>w :tabclose<CR>
" map CTRL + L (redraw screen) to also turn of search highlighting until next line
nnoremap <C-L> :nohl<CR><C-L>
" toggle NERDTree
nmap <leader>e :NERDTreeToggle<CR>
" scroll window next to current down/up
nmap <silent> <leader>j <c-w>w<c-d><c-w>W
nmap <silent> <leader>k <c-w>w<c-u><c-w>W

" CATEGORIE: FUNCTIONS
so ~/.config/dotfiles/vim/functions.vim

" CATEGORIE: DEFAULT VIMRC

" The default vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2019 Feb 18
"
" This is loaded if no vimrc file was found.
" Except when Vim is run with "-u NONE" or "-C".
" Individual settings can be reverted with ":set option&".
" Other commands can be reverted as mentioned below.

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Bail out if something that ran earlier, e.g. a system wide vimrc, does not
" want Vim to use these default values.
if exists('skip_defaults_vim')
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
" Avoid side effects when it was already reset.
if &compatible
  set nocompatible
endif

" When the +eval feature is missing, the set command above will be skipped.
" Use a trick to reset compatible only when the +eval feature is missing.
silent! while 0
  set nocompatible
silent! endwhile

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

set history=200		" keep 200 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set wildmenu		" display completion matches in a status line

set ttimeout		" time out for key codes
set ttimeoutlen=100	" wait up to 100ms after Esc for special key

" Show @@@ in the last line if it is truncated.
set display=truncate

" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=5

" Do incremental searching when it's possible to timeout.
if has('reltime')
  set incsearch
endif

" Do not recognize octal numbers for Ctrl-A and Ctrl-X, most users find it
" confusing.
set nrformats-=octal

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries.
if has('win32')
  set guioptions-=t
endif

" Don't use Ex mode, use Q for formatting.
" Revert with ":unmap Q".
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" Revert with ":iunmap <C-U>".
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine.  By enabling it you
" can position the cursor, Visually select and scroll with the mouse.
if has('mouse')
  set mouse=a
endif

" Switch syntax highlighting on when the terminal has colors or when using the
" GUI (which always has colors).
if &t_Co > 2 || has("gui_running")
  " Revert with ":syntax off".
  syntax on

  " I like highlighting strings inside C comments.
  " Revert with ":unlet c_comment_strings".
  let c_comment_strings=1
endif

" Only do this part when Vim was compiled with the +eval feature.
if 1

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  " Revert with ":filetype off".
  filetype plugin indent on

  " Put these in an autocmd group, so that you can revert them with:
  " ":augroup vimStartup | au! | augroup END"
  augroup vimStartup
    au!

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid, when inside an event handler
    " (happens when dropping a file on gvim) and for a commit message (it's
    " likely a different one than last time).
    autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif

  augroup END

endif

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
" Revert with: ":delcommand DiffOrig".
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If set (default), this may break plugins (but it's backward
  " compatible).
  set nolangremap
endif
