" CATEGORIE: FUNCTIONS

" A function that creates include guards
function IncludeGuards(guard)
    let beginGuard = "#ifndef " . a:guard . "\n#define " . a:guard
    let endGuard = "#endif //" . a:guard
    :normal gg
    put = beginGuard
    :normal G
    put = endGuard
endfunction

" A function that creates a class with constructor, destructor, private and
" public section
function CreateClass(name)
    let inputString = "class " . a:name . "\n{\npublic:\n    " . a:name . "();\n    ~" . a:name . "();\nprivate:\n};\n"
    put = inputString
endfunction
